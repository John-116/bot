import discord
from discord.ext import commands
import requests


intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix='!', intents=intents)

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name}')

@bot.event
async def on_message(message):
    if message.channel.name == 'blog':
        # Extract the message content and timestamp
        message_content = message.content
        timestamp = message.created_at.strftime("%Y-%m-%d %H:%M:%S")

        # Send the message data to your Flask app
        response = requests.post('http://localhost:5000/upload', json={'message': message_content, 'timestamp': timestamp})

        if response.status_code == 200:
            print('Message uploaded successfully to the website')

bot.run('INSERT-YOUR-API-KEY-HERE')
