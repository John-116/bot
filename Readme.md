# Blogbot

This is a discord bot which uploads all messages sent a #blog channel to an online blog.


## Directory Structure

The project is broken up into 2 components:

1. The site - This is a simple flasks site which manages the SQL database and the actual blog
2. The bot - A simple discord bot you need to insert your key where specified to allow the bot to run

## Installation

1. Download the repo
2. Install packages from the requirements files I was recommend breaking it up into 2 [venvs](https://docs.python.org/3/library/venv.html)
3. Make your [discord bot](https://discord.com/developers/docs/getting-started)
4. Insert your api key into bot.py
5. Run the site and bot
