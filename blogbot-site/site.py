from flask import Flask, request, render_template
from datetime import datetime
import sqlite3

app = Flask(__name__)

# Load or initialise database 
conn = sqlite3.connect('messages.db',check_same_thread=False)
c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS messages
             (id INTEGER PRIMARY KEY,
             timestamp TEXT NOT NULL,
             text TEXT NOT NULL)''')
conn.commit()

# Load messages from the database
c.execute("SELECT timestamp, text FROM messages")
uploaded_messages = []
for row in c.fetchall():
    uploaded_messages.append({'timestamp': row[0], 'message': row[1]})



@app.route('/upload', methods=['POST'])
def upload_message():
    # Unpack the data
    data = request.json
    message = data['message']
    timestamp = data['timestamp']

    # Format the timestamp
    formatted_timestamp = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S').strftime('%B %d, %Y %I:%M %p')

    # Store the message with the formatted timestamp
    uploaded_messages.append({'message': message, 'timestamp': formatted_timestamp})
    c.execute("INSERT INTO messages (timestamp, text) VALUES (?, ?)", (formatted_timestamp, message))
    conn.commit()

    return 'Message uploaded successfully'



@app.route('/')
def view_messages():
    return render_template('messages.html', messages=uploaded_messages)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
